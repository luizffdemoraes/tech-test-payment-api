using Moq;
using PaymentApi.Models;
using PaymentApi.Models.DTO;
using PaymentApi.Repository;
using PaymentApi.Service;

namespace PaymentApi.Test
{
    public class SalesServiceTest
    {
        private readonly Mock<ISaleRepository> _repository;
        private readonly ISalesService _service;

        public SalesServiceTest()
        {
            _repository = new Mock<ISaleRepository>();
            _service = new SalesService(_repository.Object);
        }

        [Fact]
        public void Create_New_Sale_Sucess()
        {
            var sale = MockSale();
            var saleRequestDto = NewSaleRequestDto();
            DateTime time = sale.Created;

            _repository.Setup(x => x.CreateSale(sale));

            var result = _service.CreateSales(saleRequestDto);
            var okResult = Assert.IsType<SalesResponseDto>(result);

            Assert.NotNull(result);
            Assert.IsType<SalesResponseDto>(result);
            Assert.Equal("Nike", result.Products.First().Name);
            Assert.Equal("11111111111", result.Seller.CPF);
            Assert.Equal("Teste", result.Seller.Name);
            Assert.Equal(sale.Created.Date, result.Created.Date);
        }

        [Fact]
        public void Create_Sale_Throws_Exception()
        {
            var sale = MockSale();
            var saleRequestDto = NewSaleRequestDto();

            _repository.Setup(r => r.CreateSale(It.IsAny<Sale>())).Throws(new Exception());

            Action result = () => _service.CreateSales(saleRequestDto);

            Exception exception = Assert.Throws<Exception>(result);
        }


        [Fact]
        public void Find_Sale_By_Id_Success()
        {
            var sale = MockSale();
            _repository.Setup(r => r.FindBySalesId(sale.Id)).Returns(sale);

            var result = _service.FindBySalesId(sale.Id);

            Assert.NotNull(result);
            Assert.Equal("Teste", result.Seller.Name);
        }

        [Fact]
        public void Find_Sale_By_Id_Throws_Exception()
        {
            var sale = MockSale();

            Action result = () => _service.FindBySalesId(sale.Id);

            Exception exception = Assert.Throws<Exception>(result);
        }

        [Fact]
        public void Update_Sale_Status_Success()
        {
            var sale = MockSale();
            sale.Status = EStatus.Payment_Approved;

            _repository.Setup(x => x.CreateSale(sale));
            _repository.Setup(r => r.FindBySalesId(sale.Id)).Returns(sale);
            _repository.Setup(r => r.UpdateSaleStatus(sale.Id, EStatus.Payment_Approved)).Returns(sale);

            var responseDto = _service.UpdateSaleStatus(sale.Id, EStatus.Payment_Approved);

            Assert.Equal(EStatus.Payment_Approved.GetDisplayName(), responseDto.Status);
        }

        [Fact]
        public void Update_Status_Throws_Exception()
        {
            var sale = MockSale();

            Action result = () => _service.UpdateSaleStatus(sale.Id, EStatus.Sent_To_Carrier);

            Exception exception = Assert.Throws<Exception>(result);
        }

        private SaleRequestDto NewSaleRequestDto()
        {
            return new SaleRequestDto()
            {
                Seller = new SellerDto("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Products = new List<Product>() { new Product() { Name = "Nike" } }
            };
        }
        private Sale MockSale()
        {
            return new Sale()
            {
                Seller = new Seller("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Created = DateTime.Now,
                Products = new List<Product>() { new Product() { Name = "Nike" } },
                Status = EStatus.Awaiting_Payment
            };
        }
        private SalesResponseDto MockSaleResponseDto()
        {
            return new SalesResponseDto()
            {
                Seller = new Seller("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Created = DateTime.Now,
                Products = new List<Product>() { new Product() { Name = "Nike" } },
                Status = EStatus.Awaiting_Payment.GetDisplayName()
            };
        }
    }
}