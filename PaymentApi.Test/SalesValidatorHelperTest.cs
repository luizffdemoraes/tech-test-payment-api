using PaymentApi.Helper;
using PaymentApi.Models;

namespace PaymentApi.Test
{
    public class SalesValidatorHelperTest
    {

        [Fact]
        public void Validate_Status_Awaiting_Payment_For_Payment_Approved_Sale_Sucess()
        {
            var sale = MockSale();

            EStatus status = SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Payment_Approved);

            Assert.Equal(EStatus.Payment_Approved, status);
        }

        [Fact]
        public void Validate_Status_Awaiting_Payment_For_Cancelled_Sale_Sucess()
        {
            var sale = MockSale();

            EStatus status = SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Canceled);

            Assert.Equal(EStatus.Canceled, status);
        }

        [Fact]
        public void Validate_Status_Awaiting_Paymen_Exception()
        {
            var sale = MockSale();

            Action result = () => SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Sent_To_Carrier);

            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void Validate_Status_Payment_Approved_For_Sent_To_Carrier_Sucess()
        {
            var sale = MockSale();
            sale.Status = EStatus.Payment_Approved;

            EStatus status = SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Sent_To_Carrier);

            Assert.Equal(EStatus.Sent_To_Carrier, status);
        }

        [Fact]
        public void Validate_Status_Payment_Approved_Exception()
        {
            var sale = MockSale();
            sale.Status = EStatus.Payment_Approved;

            Action result = () => SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Awaiting_Payment);

            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void Validate_Status_Payment_Approved_Canceled_Sucess()
        {
            var sale = MockSale();
            sale.Status = EStatus.Payment_Approved;

            EStatus status = SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Canceled);

            Assert.Equal(EStatus.Canceled, status);
        }

        [Fact]
        public void Validate_Status_Sent_To_Carrier_Delivered_Sucess()
        {
            var sale = MockSale();
            sale.Status = EStatus.Sent_To_Carrier;

            EStatus status = SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Delivered);

            Assert.Equal(EStatus.Delivered, status);
        }

        [Fact]
        public void Validate_Status_Sent_To_Carrier_Exception()
        {
            var sale = MockSale();
            sale.Status = EStatus.Sent_To_Carrier;

            Action result = () => SalesValidatorHelper.ValidatorStatusValidatorStatus(sale, EStatus.Canceled);

            Assert.Throws<Exception>(result);
        }


        private Sale MockSale()
        {
            return new Sale()
            {
                Seller = new Seller("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Created = DateTime.Now,
                Products = new List<Product>() { new Product() { Name = "Nike" } },
                Status = EStatus.Awaiting_Payment
            };
        }
    }
}