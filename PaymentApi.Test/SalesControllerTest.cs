using Microsoft.AspNetCore.Mvc;
using Moq;
using PaymentApi.Controllers;
using PaymentApi.Models;
using PaymentApi.Models.DTO;
using PaymentApi.Service;

namespace PaymentApi.Test
{
    public class SalesControllerTest
    {
        private readonly SalesController _controller;
        private readonly Mock<ISalesService> _service;

        public SalesControllerTest()
        {
            _service = new Mock<ISalesService>();
            _controller = new SalesController(_service.Object);
        }

        [Fact]
        public void Create_New_Sale_Sucess()
        {
            // Arrange
            var requestDto = NewSaleRequestDto();
            var responseDto = MockSaleResponseDto();

            _service.Setup(s => s.CreateSales(It.IsAny<SaleRequestDto>())).Returns(responseDto);

            // Act
            var result = _controller.CreateSale(NewSaleRequestDto());

            // Assert
            var okResult = Assert.IsType<CreatedAtActionResult>(result);
            var returnValue = Assert.IsType<SalesResponseDto>(okResult.Value);

            _service.Verify(x => x.CreateSales(It.IsAny<SaleRequestDto>()), Times.Once);
            Assert.Equal("Nike", returnValue.Products.First().Name);
            Assert.Equal("11111111111", returnValue.Seller.CPF);
            Assert.Equal("Teste", returnValue.Seller.Name);
        }

        [Fact]
        public void Find_Sale_By_Id_Success()
        {
            // Arrange
            var responseDto = MockSaleResponseDto();

            _service.Setup(s => s.FindBySalesId(1)).Returns(responseDto);

            // Act
            var result = _controller.FindBySaleId(1);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<SalesResponseDto>(okResult.Value);

            _service.Verify(x => x.FindBySalesId(It.IsAny<int>()), Times.Once);
            Assert.Equal("Nike", returnValue.Products.First().Name);
            Assert.Equal("11111111111", returnValue.Seller.CPF);
            Assert.Equal("Teste", returnValue.Seller.Name);
        }

        [Fact]
        public void Find_Sale_By_Id_NotFound()
        {
            _service.Setup(s => s.FindBySalesId(It.IsAny<int>())).Returns((SalesResponseDto)null);

            var result = _controller.FindBySaleId(It.IsAny<int>());

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);

            Assert.Equal("Sale with id 0 not found", notFoundResult.Value);
            Assert.Equal(404, notFoundResult.StatusCode);
            Assert.IsType<NotFoundObjectResult>(notFoundResult);
        }

        [Fact]
        public void Update_Sale_Status_Success()
        {
            // Arrange
            var responseDto = MockSaleResponseDto();

            _service.Setup(s => s.FindBySalesId(1)).Returns(responseDto);
            _service.Setup(s => s.UpdateSaleStatus(1, EStatus.Payment_Approved)).Returns(responseDto);

            // Act
            var result = _controller.UpdateSaleStatus(1, EStatus.Payment_Approved);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<SalesResponseDto>(okResult.Value);

            _service.Verify(x => x.UpdateSaleStatus(It.IsAny<int>(), It.IsAny<EStatus>()), Times.Once);
            Assert.Equal("Nike", returnValue.Products.First().Name);
            Assert.Equal("11111111111", returnValue.Seller.CPF);
            Assert.Equal("Teste", returnValue.Seller.Name);
        }

        [Fact]
        public void Update_Sale_By_Id_NotFound()
        {
            _service.Setup(s => s.FindBySalesId(It.IsAny<int>())).Returns((SalesResponseDto)null);

            var result = _controller.UpdateSaleStatus(It.IsAny<int>(), EStatus.Payment_Approved);

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);

            Assert.Equal("Sale with id 0 not found", notFoundResult.Value);
            Assert.Equal(404, notFoundResult.StatusCode);
            Assert.IsType<NotFoundObjectResult>(notFoundResult);
        }

        private SalesResponseDto MockSaleResponseDto()
        {
            return new SalesResponseDto()
            {
                Seller = new Seller("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Created = DateTime.Now,
                Products = new List<Product>() { new Product() { Name = "Nike" } },
                Status = EStatus.Awaiting_Payment.GetDisplayName()
            };
        }

        private SaleRequestDto NewSaleRequestDto()
        {
            return new SaleRequestDto()
            {
                Seller = new SellerDto("Teste", "teste@gmail.com", "11111111111", "1192222-2222"),
                Products = new List<Product>() { new Product() { Name = "Nike" } }
            };
        }
    }
}