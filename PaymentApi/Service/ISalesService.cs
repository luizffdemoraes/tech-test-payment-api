using PaymentApi.Models;
using PaymentApi.Models.DTO;

namespace PaymentApi.Service
{
    public interface ISalesService
    {
        SalesResponseDto CreateSales(SaleRequestDto saleDto);
        SalesResponseDto FindBySalesId(int id);
        SalesResponseDto UpdateSaleStatus(int id, EStatus status);
    }
}