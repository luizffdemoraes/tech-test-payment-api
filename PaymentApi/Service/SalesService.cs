using PaymentApi.Models;
using PaymentApi.Models.DTO;
using PaymentApi.Repository;

namespace PaymentApi.Service
{
    public class SalesService : ISalesService
    {
        private readonly ISaleRepository _repository;
        public SalesService(ISaleRepository repository)
        {
            _repository = repository;
        }

        public SalesResponseDto CreateSales(SaleRequestDto saleDto)
        {

            Sale sale = new Sale
            {
                Seller = new Seller(saleDto.Seller),
                Products = saleDto.Products,
                Created = DateTime.Now,
                Status = EStatus.Awaiting_Payment
            };

            try
            {
                _repository.CreateSale(sale);

                return new SalesResponseDto()
                {
                    Id = sale.Id,
                    Seller = sale.Seller,
                    Products = sale.Products,
                    Created = sale.Created,
                    Status = sale.Status.GetDisplayName()
                };
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public SalesResponseDto FindBySalesId(int id)
        {
            try
            {
                var sale = _repository.FindBySalesId(id);

                return new SalesResponseDto()
                {
                    Id = sale.Id,
                    Seller = sale.Seller,
                    Products = sale.Products,
                    Created = sale.Created,
                    Status = sale.Status.GetDisplayName()
                };
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public SalesResponseDto UpdateSaleStatus(int id, EStatus status)
        {
            try
            {
                var sale = _repository.UpdateSaleStatus(id, status);
                
                return new SalesResponseDto()
                {
                    Id = sale.Id,
                    Seller = sale.Seller,
                    Products = sale.Products,
                    Created = sale.Created,
                    Status = sale.Status.GetDisplayName()
                };
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}