using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Models.DTO
{
    public class SaleRequestDto
    {
        [Required(ErrorMessage = "Mandatory information")]
        public SellerDto Seller { get; set; }
        [Required(ErrorMessage = "Mandatory information")]
        public List<Product> Products { get; set; }
    }
}