using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentApi.Models.DTO
{
    public class SellerDto
    {
        [StringLength(60, MinimumLength = 3)]
        public string Name { get; set; }
        [RegularExpression(@"^([a-z]){1,}([a-z0-9._-]){1,}([@]){1}([a-z]){2,}([.]){1}([a-z]){2,}([.]?){1}([a-z]?){2,}$", ErrorMessage = "Add a valid email.")]
        public string Email { get; set; }
        [RegularExpression(@"^(((\d{3}).(\d{3}).(\d{3})-(\d{2}))?((\d{2}).(\d{3}).(\d{3})/(\d{4})-(\d{2}))?)*$", ErrorMessage = "Add a valid CPF.")]
        public string CPF { get; set; }
        [RegularExpression(@"^(?:(?:\+|00)?(55)\s?)?(?:(?:\(?[1-9][0-9]\)?)?\s?)?(?:((?:9\d|[2-9])\d{3})-?(\d{4}))$", ErrorMessage = "Add a valid Telephone.")]
        public string Telephone { get; set; }

        public SellerDto(string name, string email, string cpf, string telephone)
        {
            Name = name;
            Email = email;
            CPF = cpf;
            Telephone = telephone;
        }
    }
}