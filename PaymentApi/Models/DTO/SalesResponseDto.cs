namespace PaymentApi.Models.DTO
{
    public class SalesResponseDto
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public DateTime Created { get; set; }
        public List<Product> Products { get; set; }
        public string Status { get; set; }
    }
}