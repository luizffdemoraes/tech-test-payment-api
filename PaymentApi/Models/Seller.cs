using System.ComponentModel.DataAnnotations;
using PaymentApi.Models.DTO;

namespace PaymentApi.Models
{
    public class Seller
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CPF { get; set; }
        public string Telephone { get; set; }

        public Seller(string name, string email, string cpf, string telephone)
        {
            Name = name;
            Email = email;
            CPF = cpf;
            Telephone = telephone;
        }

        public Seller(SellerDto sellerDto)
        {
            Name = sellerDto.Name;
            Email = sellerDto.Email;
            CPF = sellerDto.CPF;
            Telephone = sellerDto.Telephone;
        }

        public Seller()
        {

        }
    }
}