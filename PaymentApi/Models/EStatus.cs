using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Models
{
    public enum EStatus
    {
        [Display(Name = "Aguardando pagamento")]
        Awaiting_Payment,

        [Display(Name = "Pagamento aprovado")]
        Payment_Approved,

        [Display(Name = "Enviado para transportadora")]
        Sent_To_Carrier,

        [Display(Name = "Entregue")]
        Delivered,

        [Display(Name = "Cancelada")]
        Canceled
    }
}