namespace PaymentApi.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public List<Product> Products { get; set; }
        public DateTime Created { get; set; }
        public EStatus Status { get; set; }
    }
}