using System;
using System.Diagnostics.CodeAnalysis;
using PaymentApi.Models;

namespace PaymentApi.Helper
{
    public class SalesValidatorHelper
    {
         public static EStatus ValidatorStatusValidatorStatus(Sale sale, EStatus newStatus)
        {
            switch (sale.Status)
            {
                case EStatus.Awaiting_Payment:
                    if (newStatus != EStatus.Payment_Approved && newStatus != EStatus.Canceled)
                    {
                        throw new Exception("Invalid option! Valid options: Payment Approved or Cancelled.");
                    }
                    break;
                case EStatus.Payment_Approved:
                    if (newStatus != EStatus.Sent_To_Carrier && newStatus != EStatus.Canceled)
                    {
                        throw new Exception("Invalid option! Valid options: Sent To Carrier or Cancelled.");
                    }
                    break;
                case EStatus.Sent_To_Carrier:
                    if (newStatus != EStatus.Delivered)
                    {
                        throw new Exception("Invalid option! Valid options: Entregue.");
                    }
                    break;
                default:
                     throw new Exception($"Unable to change from {sale.Status} to {newStatus}");
            }
            return newStatus;
        }
    }
}