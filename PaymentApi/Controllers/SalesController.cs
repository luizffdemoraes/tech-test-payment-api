using Microsoft.AspNetCore.Mvc;
using PaymentApi.Models;
using PaymentApi.Models.DTO;
using PaymentApi.Service;

namespace PaymentApi.Controllers
{
    [ApiController]
    [Route("sales")]
    [Produces("application/json")]
    public class SalesController : ControllerBase
    {
        private readonly ISalesService _service;

        public SalesController(ISalesService service)
        {
            _service = service;
        }

        /// <summary>
        /// Registro de venda.
        /// </summary>
        /// <param name="saleDto">Informar dados para registro contendo vendedor e produtos válidos.</param>
        /// <returns> Um novo registro de venda. </returns>
        /// <remarks>
        /// Exemplo de request.:
        /// {
        ///  "seller": {
        ///    "id": 0,
        ///    "name": "string",
        ///  "email": "teste@gmail.com",
        ///    "cpf": "111.111.111-11",
        ///    "telephone": "11922223333"
        ///  },
        ///  "products": [
        ///    {
        ///      "id": 0,
        ///      "name": "string"
        ///    }
        ///  ]
        ///}
        /// Observação.: 
        /// O Status da venda e fixado como Aguardando pagamento e
        /// e o Horário e Data de criação tambem e definido no momento de criação.
        /// <remarks>
        /// <response code="201"> Sucesso - Retorna o registro criado </response>
        [HttpPost]
        public IActionResult CreateSale(SaleRequestDto saleDto)
        {
            var completedSale = _service.CreateSales(saleDto);

            return CreatedAtAction(nameof(FindBySaleId), new { id = completedSale.Id }, completedSale);
        }

        /// <summary>
        /// Buscar uma venda registrada por meio do ID informado.
        /// </summary>
        /// <param name="id"> Informar o id da venda para consulta</param>
        /// <returns> Os dados do vendedor, a data do registro, a lista dos itens vendidos e o status da venda</returns>
        /// <response code="200"> Sucesso - Exibe os dados do vendedor, a data do registro, a lista dos itens vendidos e o status da venda</response>
        /// <response code="404"> Não Encontrado - Se o id não for encontrado ou for inválido </response>
        [HttpGet("{id}")]
        public IActionResult FindBySaleId(int id)
        {
            var completedSale = _service.FindBySalesId(id);

            if (completedSale is null)
                return NotFound($"Sale with id {id} not found");

            return Ok(completedSale);
        }

        /// <summary>
        /// Atualizar o status de uma venda por ID
        /// </summary>
        /// <param name="id"> Informar o id da venda para consulta</param>
        /// <param name="status"> Selecionar opção válida para alteração do status</param>
        /// <returns></returns>
        /// <remarks>
        /// 
        /// Opções válidas para alteração do status:  
        ///  
        ///     De: "Aguardando pagamento"          Para: "Pagamento aprovado" ou "Cancelada"  
        ///     De: "Pagamento aprovado"            Para: "Enviado para transportadora"  ou "Cancelada"  
        ///     De: "Enviado para transportadora"   Para: "Entregue"  
        ///  
        /// </remarks>
        /// 
        /// <response code="200"> Sucesso - Exibe os dados do vendedor, a data do registro, a lista dos itens vendidos e o status da venda (Atualizado).</response>
        /// <response code="404"> Not Found - Se o id não for encontrado ou for inválido </response>
        [HttpPatch("{id}")]
        public IActionResult UpdateSaleStatus(int id, EStatus status)
        {
            var completedSale = _service.UpdateSaleStatus(id, status);

            if (completedSale is null)
                return NotFound($"Sale with id {id} not found");

            return Ok(completedSale);
        }
    }
}