# <font color="orange">Desafio Tech Pottencial</font>

Projeto final do **Bootcamp Pottencial .NET Developer** realizado na plataforma da DIO em parceria com a Pottencial Seguradora.

## Payment API
------------------

O Projeto consiste em uma Web API construída com ASP.NET Core 6 que realiza as seguintes operações:
- Registrar uma venda
- Buscar uma venda por ID
- Atualizar o status de uma venda
  
*Exemplo de um retorno do método GET, POST e PATCH*

```json
{
  "id": 1,
  "seller": {
    "id": 1,
    "name": "Teste",
    "email": "teste@gmail.com",
    "cpf": "111.111.111-11",
    "telephone": "11911111111"
  },
  "created": "2022-11-20T03:52:59.7784961-03:00",
  "products": [
    {
      "id": 1,
      "name": "Teste"
    }
  ],
  "status": "Aguardando pagamento"
}

```

### Tecnologias:
------------------

- .NET 6
- Entity Framework Core 6
- Banco de dados em memória do EF Core 7
- Swagger

*Testes unitários*
- xUnit
- Moq

### Construção e Organização da API:
------------------

- `Models` possui as entidades:
    - `Sale` - Venda
    - `Seller` - Vendedor
    - `Product` - Produto
    - `EStatus` - para os status de venda utilizei o tipo `enum`.
        - De para dos Status:
        - Awaiting_Payment -> Aguardando pagamento
        - Payment_Approved -> Pagamento aprovado
        - Sent_To_Carrier  -> Enviado para transportadora
        - Delivered        -> Entregue
        - Canceled         -> Cancelada
    - Interfaces das classes de repositório e serviço.
- `DTOs` são as classes entidades apenas com as informações necessárias para cada método de ação da classe `Controller`. 
- Usei `Data Annotation` para validar as propriedades das classes nos diretórios  `DTOs`.

Separei a lógica de persistência das regras de negócios por meio das classes
 `Repository`, `Service` :
- `Repository` possui as classes que gerenciam o banco de dados:
    - `PaymentContext` - para criação do banco de dados e tabelas.
    - `SaleRepository` - classe que lida com o acesso e registro de dados.
- `Services`
    - `SaleServices` - lida com as regras de negócios.
    - `EnumExtensions` - classe estática que possui um método, que usa a informação no `data annotation` do enum para apresentar os dados pelo DisplayName.
`Models` e `DTOs`, e as configurações necessárias para as propriedades.
- `Controllers`
    - `SalesController` - recebe uma instância de implementação de `ISaleService`.

- `Helper`  
    - `SalesValidatorHelper` - classe com um método para validar a atualização do status.

> Testes unitários:
>
> Classe de serviço - `SalesService`  
> Classe Controller - `SalesController`  

### Cobertura do projeto.:
------------------

![Cobertura do projeto.](../Images/Cobertura.PNG)

Comando utilizado para verificar a cobertura:
- dotnet test /p:CollectCoverage=true

### Observação
------------------
- Para orientação foi adicionado comentários na classe `SalesController`. 





