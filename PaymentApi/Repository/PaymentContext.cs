using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using PaymentApi.Models;

namespace PaymentApi.Repository
{
    [ExcludeFromCodeCoverage]
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

        public DbSet<Sale> Sales {get; set;}
        public DbSet<Seller> Sellers {get; set;}
        public DbSet<Product> Products { get; set;}

         protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Sale>(table =>
            {
                table.HasKey(x => x.Id);
                table.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
                table.HasMany(x => x.Products).WithOne();
            });

            builder.Entity<Seller>(table =>
            {
                table.HasKey(x => x.Id);
                table.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
                table.Property(x => x.Name).IsRequired();
                table.Property(x => x.CPF).IsRequired();
                table.Property(x => x.Email).IsRequired();
                table.Property(x => x.Telephone).IsRequired();
            });

            builder.Entity<Product>(table =>
            {
                table.HasKey(x => x.Id);
                table.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
                table.Property(x => x.Name).IsRequired();
            });
        }
    }
}