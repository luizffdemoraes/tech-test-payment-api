using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using PaymentApi.Helper;
using PaymentApi.Models;

namespace PaymentApi.Repository
{
    [ExcludeFromCodeCoverage]
    public class SaleRepository : ISaleRepository
    {
        private readonly PaymentContext _context;

        public SaleRepository(PaymentContext context)
        {
            _context = context;
        }

        public void CreateSale(Sale sale)
        {
            _context.Add(sale);
            _context.SaveChanges();
        }

        public Sale FindBySalesId(int id)
        {

            return  _context.Sales
                    .Include(p => p.Seller)
                    .Include(p => p.Products)
                    .AsNoTracking()
                    .SingleOrDefault(p => p.Id == id);
        }

        public Sale UpdateSaleStatus(int id, EStatus newStatus)
        {
      
            var saleUpdate = FindBySalesId(id);
            saleUpdate.Status = SalesValidatorHelper.ValidatorStatusValidatorStatus(saleUpdate, newStatus);
            _context.Sales.Update(saleUpdate);
            _context.SaveChanges();
            return saleUpdate;
        }
    }
}