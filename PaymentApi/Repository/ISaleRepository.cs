using PaymentApi.Models;

namespace PaymentApi.Repository
{
    public interface ISaleRepository
    {
        void CreateSale(Sale sale);
        Sale FindBySalesId(int id);
        Sale UpdateSaleStatus(int id, EStatus status);
    }
}